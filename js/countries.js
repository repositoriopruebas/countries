var countriesArray = [];
var map;
function continents (object)
{
    $('#datasdown').hide();
    $('#datas').empty();
    $('#map').css('height','0px');
    activationMenu();
    api("region/",$(object).data('value'));
}
function activationMenu()
{
    var menues = $(".nav li");
    menues.click(function(){
        menues.removeClass("active");
        $(this).addClass("active");
    });
}
function openCountry(object)
{
    getCountries($(object).find('input[type=hidden]').attr('value'));
}
async function api (mode,paramUrl)
{
    $('#map').css('height','0px');
    $('#datasdown').hide();
    $('#datas').empty();
    var request = $.ajax({
        url: "https://restcountries.eu/rest/v2/"+mode+''+paramUrl,
        method: "GET"
    });
    request.done(function( data ) {
        // console.log(data);
        var response = JSON.parse(JSON.stringify(data));
        console.log(response);

        for(let i = 0; i < response.length; i++)
        {
            $('.language').empty();
            //console.log(response[i].capital);
            // $('#datas').append('<img width="10%;" height="auto" title="'+response[i].name+'" height="auto" src="'+response[i].flag+'">');
            setCountries(response[i]);

            $('#number-countries').html("Number countries: "+response.length);
            $('#datas').removeClass('col-md-6');

            $('#datas').addClass('row');
            $('#datas').append(
                '<div class="col-md-3">'+
                '					<div style="border:1px solid black" onclick="openCountry(this)" id="container'+response[i].alpha3Code+'" class=" columns media-body"><br>' +
                '						<ul id="datacountry">' +
                '						<li class="mt-0 font-weight-bold">Name: '+response[i].name+'</li>' +
                '						<li class="mt-0 font-weight-bold">Region: '+response[i].subregion+'</li>' +
                '						<li class="mt-0 font-weight-bold">alpha2Code: '+response[i].alpha2Code+'</li>' +
                '						<li class="mt-0 font-weight-bold">alpha3Code: '+response[i].alpha3Code+'</li>' +
                '						<li class="mt-0 font-weight-bold">Language: '+response[i].languages[0].name+'</li>' +
                '						</ul>' +
                '						<input type="hidden"  value="'+response[i].alpha3Code+'" id="idcountry'+response[i].alpha3Code+'"> '+
                '					</div>'+
                '</div>'
            );
        }
    });
    request.fail(function( error ) {
        console.log( 'Error: ' , error );
    });
}
function setCountries(object)
{
    countriesArray.push(object);
}

function getCountries(idsearch)
{

    $.each(countriesArray,function(key,value){

        if(idsearch == value.alpha3Code)
        {
            informationCountry(value);
            return false;
        }
    });
}
function informationCountry(object)
{
    $.get("server/wrapper.php/",{"obj":object},infoNames,"json");
}
function infoNames(response)
{
    $('#datasdown').show();
    $('#datas').empty();
    $('#number-countries').html('');
    $('#datas').removeClass('row');

    $('#datas').addClass('col-md-6');
    $('#datas').append(
        '<h2>'+response.name+'<h2><br><br>'+
        '<div class="row">'+
        '<div class="col-md-5">'+
        '<img width="100%" src="'+response.flag+'">'+

        '</div>'+
        '<div class="col-md-3">'+
        '<p>Names</p>'+
        '<table id="table-names" class="table table-hover">' +
        '<tr>' +
        '<th>Common</th>' +
        '<td>'+response.name+'</td>' +
        '</tr>' +
        '<tr>' +
        '<th>Demonym</th>'+
        '<td>'+response.demonym+'</td>' +
        '</tr>'+
        '<tr>' +
        '<th>Native name</th>'+
        '<td>'+response.nativeName+'</td>' +
        '</tr>'+
        '				<th> <button onclick="showHideItems()" class="btn btn-secondary dropdown-toggle" type="button"  data-target="#myList" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
        '   Translations\n' +
        '  </button></th>'+
        '<tr class="updown"><th>br</th> <td>'+response.translations.br+'</td></tr>' +
        '<tr class="updown"><th>de</th><td>'+response.translations.de+'</td></tr>' +
        '<tr class="updown"><th>es</th><td>'+response.translations.es+'</td></tr>' +
        '<tr class="updown"><th>fa</th><td>'+response.translations.fa+'</td></tr>' +
        '<tr class="updown"><th>fr</th><td>'+response.translations.fr+'</td></tr>' +
        '<tr class="updown"><th>hr</th><td>'+response.translations.hr+'</td></tr>' +
        '<tr class="updown"><th>it</th><td>'+response.translations.it+'</td></tr>' +
        '<tr class="updown"><th>ja</th><td>'+response.translations.ja+'</td></tr>' +
        '<tr class="updown"><th>nl</th><td>'+response.translations.nl+'</td></tr>' +
        '<tr  class="updown"><th>pt</th><td>'+response.translations.pt+'</td></tr>' +
        '</table>'+
        '</div>'+
        '</div>'
    );
    $('.updown').css('display','none');
    geography(response);
}
function geography(response)
{
    var tabla = $('#GeographyTable');
    var $row = $('<tr></tr>');
    var rowElements;
    var content;
    tabla.empty();
    $(tabla).html(
        '<tr><th>Region:</th><td> '+response.region+'</td></tr>'+
        '<tr><th>SubRegion:</th><td> '+response.subregion+'</td></tr>'+
        '<tr><th>Capital:</th><td> '+response.capital+'</td></tr>'+
        '<tr><th>Demonym:</th><td> '+response.demonym+'</td></tr>'+
        '<tr><th>Lat/Lng:</th><td> '+response.latlng[0]+' / '+response.latlng[1] +'</td></tr>'+
        '<tr><th>Area:</th><td> '+response.area +' km2</td></tr>'+

        '<tr id="limits" ><th >Land Borders:</th></tr>',
        (response.borders) ? rowElements = response.borders.map(function (row){
            content = $('<td style="margin-right:200px" onclick="landBorders(this)"></td>').html('<a href="#" style="font-size: 14px">'+row+'</a>');
            $row.append(content);
            return $row;
        }):content = content,
    );
    tabla.append(rowElements);

    socialData(response);
    showMap(response.latlng[0],response.latlng[1]);
    //colorCountry(response.name);
}
function landBorders(object)
{
    getCountries($(object).find('a').html());
}
function socialData(response)
{
    var tabla = $('#SocialTable');
    var $row = $('<tr></tr>');
    var rowElements;
    var content;
    tabla.empty();
    $(tabla).html(
        '<tr><th>Native name:</th><td> '+response.nativeName+'</td></tr>'+
        '<tr><th>Currency:</th><td> '+response.currencies[0].name+" "+response.currencies[0].symbol+'</td></tr>'+
        '<tr ><th>Population:</th><td> '+response.population +' ha</td></tr>'+
        '<tr><th>Timezones:</th><td> '+response.timezones[0] +'</td></tr>'+
        '<tr><th>Languages:</th><td><tr>',
        rowElements = response.languages.map(function(row){
            content = $('<td></td>').html(row.name);
            $row.append(content);
            return $row;
        }),
    );
    tabla.append(rowElements);
}

function showHideItems()
{
    if($('.updown').css('display') == 'none')
    {
        $('.updown').css('display','flex');
    }else
    {
        $('.updown').css('display','none');
    }
}
function showMap(lat,long)
{

    if(map != undefined || map != null){
        map.remove();
    }
    $('#map').css('height','200px');
    map = L.map('map',{
        center:[lat,long],
        zoom: 3
    });
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        layers: 'TOPO-OSM-WMS',
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',

    }).addTo(map);
    var countryStyle = {
        'color': "#858585",
        'weight': 2,
        'opacity': 0.6
    };
    L.control.scale().addTo(map);
    var marker = L.marker([lat, long]).addTo(map);
}

$(document).ready(function(){
    countriesArray = [];
    $('#datasdown').hide();
    $('#map').css('height','0px');
    api("","all");

});